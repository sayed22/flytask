package com.example.flytask.ui.searchForm

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.flytask.R
import com.example.flytask.data.models.responses.Airports
import com.example.flytask.ui.base.BaseActivity
import com.example.flytask.ui.lookups.LookupsActivity
import com.example.flytask.ui.travelersDailog.TravelersDialog
import com.example.flytask.utils.Const
import kotlinx.android.synthetic.main.activity_search_form.*
import kotlinx.android.synthetic.main.include_search_inputs.*
import kotlinx.android.synthetic.main.toolbar.*

class SearchFormActivity : BaseActivity<SearchFormViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_form)
        tv_title.text = getString(R.string.search)
        setUpListenets()
        initCabinSpinner()
    }

    private fun setUpListenets() {
        lay_travelers.setOnClickListener(this)
        et_from.setOnClickListener(this)
        et_to.setOnClickListener(this)
        btn_search.setOnClickListener(this)
    }

    override fun getTheViewModel(): SearchFormViewModel = SearchFormViewModel(application)

    private fun initCabinSpinner() {
        spnrSex.adapter =
            ArrayAdapter(this, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.cabin_class))
        spnrSex.setSelection(0)

        spnrSex.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.et_from -> {
                startActivityForResult(Intent(this, LookupsActivity::class.java), ORIGIN_CODE)
            }
            R.id.et_to -> {
                startActivityForResult(Intent(this, LookupsActivity::class.java), DESTINATION_CODE)
            }
            R.id.lay_travelers -> {
                showTravelersDialog("")
            }
            btn_search.id -> {
//                startActivity(Intent(this, SearchResultActivity::class.java)
//                    .putExtra(Const.SEARCH_BODY_KEY, getTheViewModel().searchBody))
            }
        }
    }


    private fun showTravelersDialog(dialogTitle: String) {
        TravelersDialog().apply {
            isCancelable = false
            title = dialogTitle
            setOnDialogClickedListener(object : TravelersDialog.OnDialogClickedListener {
                override fun onDialogClicked(adult: Int, child: Int, infant: Int) {
                    //   this@SearchFormActivity.tv_travelers_count.text = getString(R.string.passengersPlaceHolder, (adult + child + infant).toString())
                    this@SearchFormActivity.getTheViewModel().searchBody.adult = adult
                    this@SearchFormActivity.getTheViewModel().searchBody.infant = infant
                    this@SearchFormActivity.getTheViewModel().searchBody.child = child
                }
            })
        }.show(supportFragmentManager, "")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ORIGIN_CODE -> {
                    et_from.setText(data?.getParcelableExtra<Airports>(Const.AIRPORT_KEY)!!.fullName)
                    getTheViewModel().searchBody.legs!![0].origin =
                        data?.getParcelableExtra<Airports>(Const.AIRPORT_KEY)!!.code
                }
                DESTINATION_CODE -> {
                    et_to.setText(data?.getParcelableExtra<Airports>(Const.AIRPORT_KEY)!!.fullName)
                    getTheViewModel().searchBody.legs!![0].destination =
                        data?.getParcelableExtra<Airports>(Const.AIRPORT_KEY)!!.code
                }
            }
        }
    }

    companion object {
        const val ORIGIN_CODE = 1
        const val DESTINATION_CODE = 2
    }
}