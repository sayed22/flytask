package com.example.flytask.ui.lookups

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.example.flytask.R
import com.example.flytask.data.models.DataResource.Companion.FAIL
import com.example.flytask.data.models.DataResource.Companion.SUCCESS
import com.example.flytask.data.models.responses.Airports
import com.example.flytask.ui.base.BaseActivity
import com.example.flytask.utils.Const.AIRPORT_KEY
import com.example.flytask.utils.InjectionUtils
import kotlinx.android.synthetic.main.activity_loopkups.*

class LookupsActivity : BaseActivity<LookupsViewModel>() {

    private var lookupsViewAdapter: LookupsAdapter? = null

    override fun getTheViewModel(): LookupsViewModel = InjectionUtils.getLookupsViewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loopkups)
        setUpSearchTypeListener()
    }

    private fun setUpSearchTypeListener() {

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Handler().postDelayed({
                    getTheViewModel().quary = text.toString()
                    getData()
                }, TYPE_TIME)
            }

        })
    }

    private fun getData() {
        progress.visibility = View.VISIBLE
        getTheViewModel().getAirports().observe(this, Observer {
            when (it?.status) {
                SUCCESS -> {
                    lookupsViewAdapter = LookupsAdapter(
                        it.data as ArrayList<Airports>
                        , this::onItemSelectedListener
                    )
                    autoCompleteList.adapter = lookupsViewAdapter
                    progress.visibility = View.GONE

                }
                FAIL -> {
                    progress.visibility = View.GONE

                    showShortToast(R.string.something_went_wrong)
                }
            }

        })
    }

    private fun onItemSelectedListener(airport: Airports) {
        val intent = Intent()
        intent.putExtra(AIRPORT_KEY, airport)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    companion object {
        const val TYPE_TIME = 500L
    }
}