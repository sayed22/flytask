package com.example.flytask.ui.views

import android.content.Context
import android.content.res.ColorStateList
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v4.widget.ImageViewCompat
import android.util.AttributeSet
import android.view.View
import com.example.flytask.R
import kotlinx.android.synthetic.main.view_number_picker.view.*

class NumberPicker : ConstraintLayout {

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private var minValue: Int = 0

    private var maxValue: Int = Int.MAX_VALUE

    var value = 0


    lateinit var listener: OnValueChangedListener

    fun init(attrs: AttributeSet?) {
        if (!isInEditMode) {
            inflate(context, R.layout.view_number_picker, this)

            context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.NumberPicker,
                0, 0
            ).apply {

                try {

                    value = getResourceId(R.styleable.NumberPicker_initialValue, 0)
                    setMinValue(getInt(R.styleable.NumberPicker_min, 0))
                    setMaxValue(getInt(R.styleable.NumberPicker_max, Int.MAX_VALUE))
                    setExtraText(getString(R.styleable.NumberPicker_mainTextName))

                    if (value > maxValue || value < minValue) {
                        throw IllegalStateException("number picker initial value should be between min and max values")
                    }

                    if (getResourceId(R.styleable.NumberPicker_increaseBtnTintColor, 0) != 0) {
                        ImageViewCompat.setImageTintList(
                            ic_plus,
                            ColorStateList.valueOf(
                                ContextCompat.getColor(
                                    context,
                                    getResourceId(R.styleable.NumberPicker_increaseBtnTintColor, 0)
                                )
                            )
                        )
                    }

                    if (getResourceId(R.styleable.NumberPicker_decreaseBtnTintColor, 0) != 0) {
                        ImageViewCompat.setImageTintList(
                            ic_minus,
                            ColorStateList.valueOf(
                                ContextCompat.getColor(
                                    context,
                                    getResourceId(R.styleable.NumberPicker_decreaseBtnTintColor, 0)
                                )
                            )
                        )
                    }

                    if (getResourceId(R.styleable.NumberPicker_decreaseBtnBackground, 0) != 0) {
                        ic_minus.setBackgroundResource(getResourceId(R.styleable.NumberPicker_decreaseBtnBackground, 0))
                    }

                    if (getResourceId(R.styleable.NumberPicker_increaseBtnBackground, 0) != 0) {
                        ic_plus.setBackgroundResource(getResourceId(R.styleable.NumberPicker_increaseBtnBackground, 0))
                    }


                } finally {
                    recycle()
                }

                ic_minus.setOnClickListener {
                    if (value > minValue) {
                        value--
                        countTV.text = value.toString()
                        if (::listener.isInitialized) {
                            listener.onValueChanged(value, -1)
                        }
                    }
                }
                ic_plus.setOnClickListener {
                    if (value < maxValue) {
                        value++
                        countTV.text = value.toString()
                        if (::listener.isInitialized) {
                            listener.onValueChanged(value, 1)
                        }
                    }
                }
                countTV.text = value.toString()
            }
        }
    }

    fun setMinValue(min: Int) {
        minValue = min
        if (value < min) {
            value = min
        }
        countTV.text = value.toString()
    }

    fun setMaxValue(max: Int) {
        maxValue = max
        if (value > max) {
            value = max
        }
        countTV.text = value.toString()
    }

    fun setExtraText(text: String) {
        tv_main_text.text = text
    }


    interface OnValueChangedListener {
        fun onValueChanged(value: Int, sign: Int)
    }

}