package com.example.flytask.ui.lookups

import android.app.Application
import android.arch.lifecycle.LiveData
import com.example.flytask.ui.base.BaseViewModel
import com.example.flytask.data.models.DataResource
import com.example.flytask.data.models.responses.Airports
import com.example.flytask.data.repositories.LookupRepository

class LookupsViewModel(app: Application, val lookupRepository: LookupRepository) : BaseViewModel(app) {
    lateinit var quary: String

    fun getAirports(): LiveData<DataResource<Airports>> {
        return lookupRepository.getAirports(quary)

    }
}