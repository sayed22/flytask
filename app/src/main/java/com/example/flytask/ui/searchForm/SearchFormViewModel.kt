package com.example.flytask.ui.searchForm

import android.app.Application
import com.example.flytask.data.models.bodies.search.SearchBody
import com.example.flytask.ui.base.BaseViewModel

class SearchFormViewModel (app: Application) : BaseViewModel(app) {

    val searchBody = SearchBody()
}