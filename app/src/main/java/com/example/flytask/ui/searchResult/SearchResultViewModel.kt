
package com.example.flytask.ui.searchResult

import android.app.Application
import android.arch.lifecycle.LiveData
import com.example.flytask.data.models.DataResource
import com.example.flytask.data.models.bodies.search.SearchBody
import com.example.flytask.data.models.responses.search.SearchResponse
import com.example.flytask.data.repositories.SearchRepository
import com.example.flytask.ui.base.BaseViewModel

class SearchResultViewModel(app: Application, val searchRepository: SearchRepository) : BaseViewModel(app) {

    var searchBody =SearchBody()

    fun search(): LiveData<DataResource<SearchResponse>> {
        return searchRepository.search(searchBody)

    }
}