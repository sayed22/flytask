package com.example.flytask.ui.travelersDailog

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.flytask.R
import kotlinx.android.synthetic.main.dialog_travelers.*

class TravelersDialog : DialogFragment(), View.OnClickListener {

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            btn_cancel.id -> {
                dismiss()
            }
            btn_save.id -> {
                callback!!.onDialogClicked(adult_number_picker.value, child_number_picker.value, infant_number_picker.value)
                dismiss()

            }
        }

    }

    protected var callback: OnDialogClickedListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_travelers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_cancel.setOnClickListener(this)
        btn_save.setOnClickListener(this)
        adult_number_picker.minimumHeight = 1


    }

    fun setOnDialogClickedListener(l: OnDialogClickedListener) {
        callback = l
    }

    interface OnDialogClickedListener {
        fun onDialogClicked(adult: Int, child: Int, infant: Int)
    }

}