package com.example.flytask.ui.searchResult

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.example.flytask.R
import com.example.flytask.ui.base.BaseActivity
import com.example.flytask.ui.searchForm.SearchFormViewModel
import com.example.flytask.utils.Const.SEARCH_BODY_KEY
import com.example.flytask.utils.InjectionUtils

class SearchResultActivity : BaseActivity<SearchResultViewModel>() {


    override fun getTheViewModel(): SearchResultViewModel = InjectionUtils.getSearchViewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_result)
        getDataFromIntent()
        getData()
    }

    private fun getData() {
        getTheViewModel().search().observe(this, Observer {

        })
    }

    private fun getDataFromIntent() {
       // getTheViewModel().searchBody = intent.getParcelableExtra(SEARCH_BODY_KEY)
    }

}

