package com.example.flytask.ui.lookups

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.flytask.R
import com.example.flytask.data.models.responses.Airports
import com.example.flytask.utils.extensions.inflateView
import kotlinx.android.synthetic.main.item_lookup.view.*
import kotlinx.android.synthetic.main.view_number_picker.view.*

class LookupsAdapter(
    private val lookups: ArrayList<Airports>,
    private val selectListener: (Airports) -> Unit
) : RecyclerView.Adapter<LookupsAdapter.LookupsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): LookupsViewHolder {
        return LookupsViewHolder(parent.inflateView(R.layout.item_lookup))
    }

    override fun getItemCount(): Int = lookups.size

    override fun onBindViewHolder(holder: LookupsViewHolder, p1: Int) {
        lookups[p1].let {
            holder.itemView.tv_name?.text = it.fullName
        }
    }


    inner class LookupsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                selectListener.invoke(lookups[adapterPosition])
            }
        }
    }
}