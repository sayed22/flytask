package com.example.flytask.ui.base

import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import android.widget.TextView
import com.example.flytask.R
import com.example.flytask.utils.Utils
import kotlinx.android.synthetic.main.toolbar.*


abstract class ParentActivity : AppCompatActivity(), View.OnClickListener {
    private var rootView: FrameLayout? = null
    private var menuId: Int = 0
    private var enableBack: Boolean = false
    private var iconResId: Int = 0
    protected lateinit var activity: AppCompatActivity

    var returnArrowResId = -1
    var returnArrowColor = -1
    open var returnArrowClickListener = View.OnClickListener {
        existActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent)
        activity = this
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }


    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        if (toolbar != null) {
            if (enableBack) {
//                returnArrow.visibility = View.VISIBLE
//                if (returnArrowResId != -1)
//                    returnArrow.setImageResource(returnArrowResId)
//                if (returnArrowColor != -1)
//                    returnArrow.setColorFilter(
//                        ContextCompat.getColor(this, returnArrowColor),
//                        android.graphics.PorterDuff.Mode.SRC_IN
//                    )
//
//                returnArrow.setOnClickListener(returnArrowClickListener)
            }
        }
    }

    fun existActivity() {
        finish()
    }

    fun getResColor(id: Int): Int {
        return ContextCompat.getColor(this, id)
    }

    fun loadFragment(container: Int, fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(container, fragment)
            .commitAllowingStateLoss()
    }

    fun hideKeyboard() {
        if (rootView != null) {
            Utils.hideKeyboard(rootView!!)
        }
    }

    override fun onClick(p0: View?) {
    }

    override fun setTitle(title: CharSequence) {
        if (toolbar != null) {
            val strArray = title.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val builder = StringBuilder()
            for (s in strArray) {
                val cap = s.substring(0, 1).toUpperCase() + s.substring(1)
                builder.append("$cap ")
            }
            //titleText.text = builder.toString()
        }
    }


    fun enableBackButton() {
        enableBack = true
    }

    fun hasInternetConnection(): Boolean {
        return Utils.hasConnection(this)
    }
}

