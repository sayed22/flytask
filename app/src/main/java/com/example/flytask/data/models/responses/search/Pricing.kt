package com.example.flytask.data.models.responses.search

import com.google.gson.annotations.SerializedName

data class Pricing(@SerializedName("total")
                   val total: String) {
}