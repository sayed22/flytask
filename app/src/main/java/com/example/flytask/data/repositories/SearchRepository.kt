package com.example.flytask.data.repositories

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.flytask.data.apis.search.SearchWebService
import com.example.flytask.data.models.DataResource
import com.example.flytask.data.models.bodies.search.SearchBody
import com.example.flytask.data.models.responses.search.SearchResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchRepository(private val searchWebService: SearchWebService) {

    fun search(body: SearchBody): LiveData<DataResource<SearchResponse>> {
        val liveData = MutableLiveData<DataResource<SearchResponse>>()
        val callable = searchWebService.search(body)
        callable.enqueue(SearchCustomResponse(liveData))
        return liveData
    }
}


class SearchCustomResponse(val liveData: MutableLiveData<DataResource<SearchResponse>>) : Callback<SearchResponse> {
    private val dataResource = DataResource<SearchResponse>()

    override fun onFailure(call: Call<SearchResponse>?, t: Throwable?) {
        dataResource.status = DataResource.FAIL
        liveData.value = dataResource
    }

    override fun onResponse(call: Call<SearchResponse>?, response: Response<SearchResponse>?) {
        if (response?.code() == 200) {
            dataResource.status = DataResource.SUCCESS
            dataResource.data = response.body() as List<SearchResponse>
        } else {
            dataResource.status = DataResource.FAIL
        }
        liveData.value = dataResource
    }

}