package com.example.flytask.data.apis.lookup

import com.example.flytask.data.apis.WebserviceManagerImpl
import com.example.flytask.data.models.responses.Airports
import okhttp3.ResponseBody
import retrofit2.Call

class LookupWebService(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun getLookups(query:String): Call<List<Airports>> {
        return webserviceManagerImpl.createService(LookupsApi::class.java).lookups(query)
    }
}