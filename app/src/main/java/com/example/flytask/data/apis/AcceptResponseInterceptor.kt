package com.example.flytask.data.apis

import okhttp3.Interceptor
import okhttp3.Response

class AcceptResponseInterceptor(private val acceptType:String):Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain?.request()
        val builder = original?.newBuilder()?.header("Accept",acceptType)?.header("Authorization","guMRjevTJNNgv49LRTNCTzfp9cWnW6Sj")
        return chain!!.proceed(builder?.build())
    }
}