package com.example.flytask.data.models.responses.search

import com.google.gson.annotations.SerializedName

data class Itineraries(
    @SerializedName("itineraryId")
    val itineraryId: String,
    @SerializedName("pricing")
    val pricing: Pricing,
    @SerializedName("carrier")
    val Carrier: Pricing,
    @SerializedName("legs")
    val legsdta: List<String>

)