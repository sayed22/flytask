package com.example.flytask.data.apis

import com.example.flytask.data.models.DataResource
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException
import com.google.gson.reflect.TypeToken


open class CustomResponse<T>(val onResponse: (DataResource<T>) -> Unit) : Callback<ResponseBody> {
    val dataResource = DataResource<T>()

    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
        dataResource.status = DataResource.FAIL
        if (t is UnknownHostException) {
            dataResource.status = DataResource.FAIL_NO_INTERNET
        } else if (t is TimeoutException) {
            dataResource.status = DataResource.FAIL_TIME_OUT
        }
        onResponse.invoke(dataResource)
    }

    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
        dataResource.status = response.code().toString()
        if (response.body() != null) {
            when (response.code().toString()) {
                DataResource.SUCCESS -> {
                    val data = response.body()!!.toString()
                    val gson = Gson()
                    val userType = object : TypeToken<T>() {
                    }.type
                    val list = gson.fromJson<List<T>>(data, userType)

                    dataResource.data = list
                }
                DataResource.VALIDATION_ERROR -> {
                    //  dataResource.message = response.body()!!.message
                }
            }
        } else {
            dataResource.status = DataResource.INTERNAL_SERVER_ERROR
        }
        onResponse.invoke(dataResource)
    }
}