package com.example.flytask.data.apis

import retrofit2.Retrofit
import okhttp3.OkHttpClient
import android.text.TextUtils
import android.net.Uri
import android.util.Log
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Query


open class WebserviceManagerImpl : WebServiceManger {
    private lateinit var retrofit: Retrofit
    private var retrofitBuilder: Retrofit.Builder = Retrofit.Builder()
    private var httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    private var authenticationInterceptor: AuthenticationInterceptor? = null

    constructor() {
        retrofitBuilder.baseUrl(BASE_URL)
        val contentTypeInterceptor = AcceptResponseInterceptor("application/json")
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create())
        if (!httpClient.interceptors().contains(contentTypeInterceptor)) {
            httpClient.addInterceptor(contentTypeInterceptor)
        }
    }


    override fun <S> createService(serviceClass: Class<S>, token: String): S {
        if (!TextUtils.isEmpty(token)) {
            authenticationInterceptor = AuthenticationInterceptor(token)
            if (!httpClient.interceptors().contains(authenticationInterceptor)) {
                httpClient.addInterceptor(authenticationInterceptor)
            }
        }
        retrofitBuilder.client(httpClient.build())
        retrofit = retrofitBuilder.build()
        return retrofit.create(serviceClass)
    }

    override fun <S> createService(serviceClass: Class<S>): S {
        retrofitBuilder.client(httpClient.build())

        retrofit = retrofitBuilder.build()

        return retrofit.create(serviceClass)
    }

    companion object {
        private var webserviceManagerImpl: WebserviceManagerImpl? = null
        const val SERVER_CODE_200 = 200
        const val SERVER_CODE_500 = 500
        const val SERVER_CODE_422 = 422

        fun instance(): WebserviceManagerImpl {
            if (webserviceManagerImpl == null) {
                webserviceManagerImpl = WebserviceManagerImpl()
            }
            return webserviceManagerImpl!!
        }

        var BASE_URL = "https://api.fly365.com/"

    }
}