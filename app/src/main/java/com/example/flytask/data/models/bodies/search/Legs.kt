package com.example.flytask.data.models.bodies.search

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Legs(
    @SerializedName("origin")
    var origin: String = "",
    @SerializedName("destination")
    var destination: String = "",
    @SerializedName("departureDate")
    var departureDate: String = "2019-06-20"
): Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(origin)
        parcel.writeString(destination)
        parcel.writeString(departureDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Legs> {
        override fun createFromParcel(parcel: Parcel): Legs {
            return Legs(parcel)
        }

        override fun newArray(size: Int): Array<Legs?> {
            return arrayOfNulls(size)
        }
    }

}