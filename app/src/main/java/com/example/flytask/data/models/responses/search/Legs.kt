package com.example.flytask.data.models.responses.search

import com.google.gson.annotations.SerializedName

data class Legs(@SerializedName("origin")
                val origin: String,
                @SerializedName("destination")
                val destination: String)