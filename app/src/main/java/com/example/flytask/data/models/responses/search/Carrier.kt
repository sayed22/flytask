package com.example.flytask.data.models.responses.search

import com.google.gson.annotations.SerializedName

data class Carrier(@SerializedName("name")
                   val name: String)