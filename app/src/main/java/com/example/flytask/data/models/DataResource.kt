package com.example.flytask.data.models

import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class DataResource<T> {

    var data: List<T>? = null
    var status: String = ""
    var message: String = ""




    companion object {
        const val SUCCESS = "200"
        const val FAIL = "Fail"
        const val FAIL_NO_INTERNET = "noInternet"
        const val FAIL_TIME_OUT = "timeOut"
        const val INTERNAL_SERVER_ERROR = "500"
        const val VALIDATION_ERROR = "400"
        const val AUTH_ERROR = "403"
    }
}