package com.example.flytask.data.apis.lookup

import com.example.flytask.data.models.DataResource
import com.example.flytask.data.models.responses.Airports
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface LookupsApi {
    @GET("suggest/airport/search")
    fun lookups(@Query("q") query: String): Call<List<Airports>>
}