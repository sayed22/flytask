package com.example.flytask.data.apis.search

import com.example.flytask.data.models.bodies.search.SearchBody
import com.example.flytask.data.models.responses.Airports
import com.example.flytask.data.models.responses.search.SearchResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET

interface SearchApi {

    @GET("flight/search")
    fun search(@Body searchBody: SearchBody): Call<SearchResponse>
}