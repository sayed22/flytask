package com.example.flytask.data.repositories

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.flytask.data.apis.CustomResponse
import com.example.flytask.data.apis.lookup.LookupWebService
import com.example.flytask.data.models.DataResource
import com.example.flytask.data.models.responses.Airports
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LookupRepository(private val lookupsWebservice: LookupWebService) {

    fun getAirports(quary: String): LiveData<DataResource<Airports>> {
        val liveData = MutableLiveData<DataResource<Airports>>()
        val callable = lookupsWebservice.getLookups(quary)
        callable.enqueue(LookupsResponse(liveData))
        return liveData
    }
}


class LookupsResponse(val liveData: MutableLiveData<DataResource<Airports>>) : Callback<List<Airports>> {
    private val dataResource = DataResource<Airports>()

    override fun onFailure(call: Call<List<Airports>>?, t: Throwable?) {
        dataResource.status = DataResource.FAIL
        liveData.value = dataResource
    }

    override fun onResponse(call: Call<List<Airports>>?, response: Response<List<Airports>>?) {
         if (response?.code() == 200) {
            dataResource.status = DataResource.SUCCESS
            dataResource.data = response.body()
        } else {
            dataResource.status = DataResource.FAIL
        }
        liveData.value = dataResource
    }

}