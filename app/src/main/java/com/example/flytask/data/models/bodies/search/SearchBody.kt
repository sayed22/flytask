package com.example.flytask.data.models.bodies.search

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class SearchBody(
    @SerializedName("legs")
    var legs: List<Legs> = ArrayList(),
    @SerializedName("infant")
    var infant: Int = 0,
    @SerializedName("infant")
    var child: Int = 0,
    @SerializedName("infant")
    var adult: Int = 1,
    @SerializedName("cabinClass")
    var cabinClass: String = "Economy") : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(Legs),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(legs)
        parcel.writeInt(infant)
        parcel.writeInt(child)
        parcel.writeInt(adult)
        parcel.writeString(cabinClass)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchBody> {
        override fun createFromParcel(parcel: Parcel): SearchBody {
            return SearchBody(parcel)
        }

        override fun newArray(size: Int): Array<SearchBody?> {
            return arrayOfNulls(size)
        }
    }

}