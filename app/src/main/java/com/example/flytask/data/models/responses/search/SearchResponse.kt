package com.example.flytask.data.models.responses.search

import com.google.gson.annotations.SerializedName

data class SearchResponse(@SerializedName("itineraries")
                          val itineraries: List<Itineraries>,
                          @SerializedName("legs")
                          val legs: List<Legs>)