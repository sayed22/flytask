package com.example.flytask.data.apis.search

import com.example.flytask.data.apis.WebserviceManagerImpl
import com.example.flytask.data.models.bodies.search.SearchBody
import com.example.flytask.data.models.responses.search.SearchResponse
import retrofit2.Call

class SearchWebService(private val webserviceManagerImpl: WebserviceManagerImpl) {

    fun search(body: SearchBody): Call<SearchResponse> {
        return webserviceManagerImpl.createService(SearchApi::class.java).search(body)
    }
}