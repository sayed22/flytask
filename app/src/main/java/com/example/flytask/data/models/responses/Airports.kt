package com.example.flytask.data.models.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Airports(
    @SerializedName("fullName")
    val fullName: String,
    @SerializedName("code")
    val code: String): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(fullName)
        parcel.writeString(code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Airports> {
        override fun createFromParcel(parcel: Parcel): Airports {
            return Airports(parcel)
        }

        override fun newArray(size: Int): Array<Airports?> {
            return arrayOfNulls(size)
        }
    }

}