package com.example.flytask.data.apis


interface WebServiceManger {
     fun <S> createService(serviceClass: Class<S>, token: String): S

    fun <S> createService(serviceClass: Class<S>): S
}