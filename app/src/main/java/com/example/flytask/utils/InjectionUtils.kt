package com.example.flytask.utils

import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import com.example.flytask.data.apis.WebserviceManagerImpl
import com.example.flytask.data.apis.lookup.LookupWebService
import com.example.flytask.data.apis.search.SearchWebService
import com.example.flytask.data.repositories.LookupRepository
import com.example.flytask.data.repositories.SearchRepository
import com.example.flytask.ui.base.ViewModelsCustomProvider
import com.example.flytask.ui.lookups.LookupsViewModel
import com.example.flytask.ui.searchResult.SearchResultViewModel

object InjectionUtils {


    fun getLookupsViewModel(activity: FragmentActivity): LookupsViewModel {
        val lookupWebService = LookupWebService(WebserviceManagerImpl.instance())
        val lookupRepository = LookupRepository(lookupWebService)
        return ViewModelProviders.of(
            activity
            , ViewModelsCustomProvider(LookupsViewModel(activity.application, lookupRepository))
        ).get(LookupsViewModel::class.java)
    }

    fun getSearchViewModel(activity: FragmentActivity): SearchResultViewModel {
        val searchWebService = SearchWebService(WebserviceManagerImpl.instance())
        val searchRepository = SearchRepository(searchWebService)
        return ViewModelProviders.of(
            activity
            , ViewModelsCustomProvider(SearchResultViewModel(activity.application, searchRepository))
        ).get(SearchResultViewModel::class.java)
    }

}